import { useState } from "react";
import ReactDOM from "react-dom";
// import { useStoreState } from "pullstate";
import { UIStore } from "../stores/UIStore";


export default function JesseStore() {
  const userName = UIStore.useState(s => s.userName)

    
    function Component1() {
      const userName = UIStore.useState(s => s.userName)
        return (
          <>
            <h1>{`Hello ${userName}!`}</h1>
            <Component2/>
          </>
        );
      }
      
      function Component2({ user }) {
        return (
          <>
            <h1>Component 2</h1>
            <Component3/>
          </>
        );
      }
      
      function Component3({ user }) {
        return (
          <>
            <h1>Component 3</h1>
            <Component4/>
          </>
        );
      }
      
      function Component4({ user }) {
        const isDarkMode = UIStore.useState(s => s.isDarkMode);
        return (
          <>
            <h1>Component 4</h1>
            <button
        onClick={() =>
          UIStore.update(s => {
            s.isDarkMode = !isDarkMode;
            console.log('Darkmode', isDarkMode)
          })
        }>
        Toggle Dark Mode
      </button>
      <Component5/>
          </>
        );
      }
      
      function Component5({ user }) {
        const userName = UIStore.useState(s => s.userName)

        return (
          <>
            <h1>Component 5</h1>
            <h1>{`Hello ${userName} again!`}</h1>
          </>
        );
      }
      return (
        <>
          <h1>{`Hello ${userName}!`}</h1>
          <Component1 user={userName} />
        </>
      );
}
