import { useReducer } from "react";
import { useEffect } from "react";


export default function ToDoWithReduceHook() {
    const initialTodos = [
        {
          id: 1,
          title: "Todo 1",
          complete: false,
        },
        {
          id: 2,
          title: "Todo 2",
          complete: false,
        },
      ];
          
      const reducer = (state, action) => {
        switch (action.type) {
          case "COMPLETE":
            return state.map((todo) => {
              if (todo.id === action.id) {
                return { ...todo, complete: !todo.complete };
              } else {
                return todo;
              }
            });
            case "UPDATE":
                return state.map((todo) => {
                    console.log("boum")
                    if (todo.id === action.id) {
                      return { ...todo, title: action.title };
                    } else {
                      return todo;
                    }
                });
            case "ADD": {
                console.log(state)

                return [...state, {          id: state.length+1,
                    title: "",
                    complete: false}]
            }              
          default:
            return state;
        }
      };
    function Todos() {
        const [todos, dispatch] = useReducer(reducer, initialTodos);
        // useEffect(() => {
        //     setTimeout(() => {
        //       setCount((count) => count + 1);
        //     }, 1000);
        //   });
        
        const handleComplete = (todo) => {
            dispatch({ type: "COMPLETE", id: todo.id });
        }
        const updateTitle = (todo, e) => {
            dispatch({ type: "UPDATE", id: todo.id, title: e.target.value });
        }
        const addTodo = (todo) => {
            dispatch({ type: "ADD" });
        };
        console.log("todos:", todos)
    
        return (
            <>
            {todos.map((todo) => (
                <div key={todo.id}>
                <label>
                    <input
                    type="checkbox"
                    checked={todo.complete}
                    onChange={() => handleComplete(todo)}
                    />
                    {todo.title}
                </label>
                <input
                    type="text"
                    value={todo.title}
                    onChange={(e) => updateTitle(todo, e)}
                    />

                </div>
            ))}
            <button onClick = {() => {addTodo()}}>Add todo</button>
            </>
        );
    }
    return (
        Todos()
    )
}