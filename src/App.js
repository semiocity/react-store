import logo from './logo.svg';
// import './App.css';
import JesseStore from './layout/JesseStore';
import ToDoWithReduceHook from './layout/ExoReduce';
import { UIStore } from "./stores/UIStore";


function App() {
  const isDarkMode = UIStore.useState(s => s.isDarkMode);

  return (
  <div style={{
              background: isDarkMode ? "black" : "white",
              color: isDarkMode ? "white" : "black",
            }}>
              <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <JesseStore/>
        <ToDoWithReduceHook/>
      </header>
    </div>
  );
}

export default App;
